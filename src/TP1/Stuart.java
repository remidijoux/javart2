package TP1;

public class Stuart extends Minion {


//D�claration des attributs
	
	//Attribut entier production qui va d�finir une production comprise entre 15 et 25
	public int production = 15 +(int)(Math.random() * ((25-15)+1));
	//Attribut double salaire qui calcul le salaire en fonction de la production
	public double salaire = production*(100 + bourse)/100;
	//Attribut entier static qui va fournir un id pour la cr�ation de stuart 
	public static int id =1;



	//On a la constructeur qui aura un attribut nom pour d�fnir le nom des futurs objet
	public Stuart() {
		nom = "Stuart " + id;
		id++;


	}


	//On cr�e une m�thode pour faire travailler les Stuarts 
	public void travailler() {

		//On fait une boucle while qui permettra de faire travailler le Stuart jusqu'a que sa retraite atteint 0
		while(retraite > 0) {

		
			retraite = retraite-1;
			bourse = salaire + bourse;
			annee = annee+1;	
			System.out.println("La retraite de "+getNom()+" est dans "+retraite+"ans");



		}
	}

	//On fait une m�thode pour faire travailler le Stuart une fois
	public void travaillerunefois() {

		retraite = retraite-1;
		bourse = salaire + bourse;
		annee = annee+1;	
		System.out.println("La retraite de "+getNom()+" est dans "+retraite+"ans");

		//Si le Stuart va � la retraite on enl�ve de l'argent de la bourse
		if (retraite == 0) {
			this.bourse = bourse - (bourse * 0.2);

		}

	}

	//On fait une m�thode pour faire travailler des Stuarts pendant 5ans
	public void cinq_ans() {

		while (annee < 5) {
			travailler();	
		}


	}



}
