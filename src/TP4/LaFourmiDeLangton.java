package TP4;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LaFourmiDeLangton  extends JFrame implements ActionListener{
	
//Attributs
	//On cr�e un panneau qui contiendra toute la fenetre
	private Container panneau;
	//On cr�e un Jbutton pour le bouton next
	private JButton monBouton;
	//On cr�e un label qui calculera le nombre de tours
	private JLabel nbTour;
	//On cr�e un jtextfield pour un plateau
	private JTextField plateau[][];

	//On cr�e une variable dimension pour la taille du plateau
	private int dimension=100;
	//On cr�e deux variable x et y pour donner les coordonnn�es de la fourmi
	private int x = dimension/2;
	private int y = dimension/2;
	//On cr�e une variable position pour donner la position de la fourmi
	private int position=0;
	//On cr�e un entier direction pour savoir orienter la fourmis
	private int direction=0;
	//On cr�e un compteur pour calculer le nombre de tour
	private int compt;


	public LaFourmiDeLangton (){
		//Cr�ation de la fen�tre
		this.setTitle(" La Fourmi de Langton");
		this.setSize(1500, 1000);
		//On d�finit la location de la fenetre
		this.setLocation(300,200);
		//Cela va permettre de terminer l'application  la fermeture de la fentre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//R�cup�ration du container
		panneau = getContentPane();


		//Cr�ation des objets � utiliser
		nbTour = new JLabel();
		monBouton = new JButton("Next");




		//Ajoute aux pannels

		panneau.add(nbTour, BorderLayout.NORTH);
		panneau.add(this.creerPlateau() ,BorderLayout.CENTER);
		panneau.add(monBouton, BorderLayout.SOUTH);

		this.initFourmi();

		//On met la possibilit� de lire le bouton
		monBouton.addActionListener(this);

		setVisible(true);
		this.automatic();


	}

	public static void main(String[] args) {
		LaFourmiDeLangton  n = new LaFourmiDeLangton ();


	}
	
	
	//On a une m�thode qui permettra d'effectuer une action lorsque que l'on clique sur mon bouton
	public void actionPerformed(ActionEvent e) {
		
		//On choisi quel action sur quel bouton. Dans ce cas si c'est "monBouton"
		if (e.getSource() == monBouton) {
			traiterMonBouton();
			for(int i=0;i<1000;i++) {
				//next();
			}
		}
	}

	//On cr�e une m�thode pour cr�e le plateau
	public JPanel creerPlateau() {
		//On ajoute un panel dans le tableau
		JPanel p = new JPanel();
		//On d�finit la dimension de la grille
		p.setLayout(new GridLayout(dimension, dimension));
		
		//On cr�e le plateau
		plateau = new JTextField[dimension][dimension];


		//On met deux conditions pour remplir la grille
		for(int i=0; i <dimension; i++) {
			for(int j=0; j <dimension; j++) {

			
				JTextField s = new JTextField(" ");
				plateau[i][j] = s;
				plateau[i][j].setBackground(Color.WHITE);
				//On ajoute le label au plateau
				p.add(s);

			}

		}
		return p;
		
	}
	//On d�finit dans cette m�thode la position de la fourmis
	public void initFourmi() {
		plateau[x][y].setText(""+position);	
	}
	//On d�finit une m�thode next pour d�placer la fourmis
	public void next() {
		//On ajoute qui changera la couleur de la case en noir si elle est blanche
		if(plateau[x][y].getBackground()== Color.WHITE) {
			plateau[x][y].setBackground(Color.BLACK);

			//On ajoute plusieurs conditions pour d�placer la fourmis en fonction de sa direction
			if(direction == 0) {
				direction=270;	
				y = y-1;
				compt++;
			}
			else if(direction == 90) {
				direction=0;
				x = x-1;
				compt++;
			}
			else if(direction ==180) {
				direction=90;
				y=y+1;
				compt++;
			}
			else if(direction ==270) {
				direction=180;
				x=x+1;
				compt++;
			}		 	
		}
		//On ajotue une condition pour changer la couleur de la case en blanche si elle est blanche
		if(plateau[x][y].getBackground()==Color.BLACK) {
			plateau[x][y].setBackground(Color.WHITE);


			//On ajoute �galements des conditions pour d�placer la fourmis en fonction de sa direction
			if(direction == 0) {
				direction=90;	
				y = y+1;
				compt++;
			}
			else if(direction == 90) {
				direction=180;
				x = x+1;
				compt++;
			}
			else if(direction ==180) {
				direction=270;
				y=y-1;
				compt++;
			}
			else if(direction ==270) {
				direction=0;
				x=x-1;
				compt++;
			}
		}
		//On affiche le nombre de tour
		plateau[x][y].setText(""+direction);
		nbTour.setText("Nombre de tours : "+compt);

	}

	//On cr�e une m�thode automatic pour faire en sorte que la fourmis ne d�pace pas le plateau et enl�ver l'erreur
	public void automatic() {
		while(x+1<dimension || y+1<dimension || x-1<0 || y-1<0) {			
			
			try {
				next();
				Thread.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	//M�thode pour v�rifier que le bouton fonctionne
	public void traiterMonBouton(){
		System.out.println("Click");
	}




}
