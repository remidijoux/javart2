package TP3;

public class Compteur1bis implements Runnable{
	
	private String nom;

	public Compteur1bis(String nom) {
		this.nom=nom;
	}


	public void run() {
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}

	public static void  main(String args[]){
		Compteur1bis t1, t2, t3;
		t1=new Compteur1bis("Hello ");
		t2=new Compteur1bis("World ");
		t3=new Compteur1bis("and Everybody ");
		
		Thread my_thread = new Thread(t1);
		Thread my_thread2 = new Thread(t2);
		Thread my_thread3 = new Thread(t3);
		
		my_thread.start();
		my_thread2.start();
		my_thread3.start();
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Fin du programme");
		
		System.exit(0);

	}

}
