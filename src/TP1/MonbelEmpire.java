package TP1;

import java.util.Scanner; //Ceci nous permettra de capturer le clavier


public class MonbelEmpire {

	public static void main(String[] args) {

		//On utilise la fonction scan pour capturer le clavier
		Scanner scan = new Scanner(System.in);

		//On cr�e un objet Usine
		Usine u1 = new Usine("Adidas");
		//On cr�e un objet Stuart
		Stuart a = new Stuart();
		
		//On ajoute un Stuart dans l'usine
		u1.ajouterdansusine(a);

		//On met en place une boucle for pour d�clencher diff�rente action en fonction du chiffre choisi
		for (int i=0; i<5; i++) {	

			System.out.println("Appuiez sur 1 pour  Sonner la cloche du travail");
			System.out.println("Appuiez sur 2 pour  Collecter les imp�ts");
			System.out.println("Appuiez sur 3 pour  Recruter un Stuart pour 50 grosses pi�ces");

			int action;
			action = scan.nextInt();

			if(action == 1) {	
				u1.travaillerStuartSansImpot();
				System.out.println("La valeur du coffre est : "+u1.getCoffre());
			}
			else if(action == 2) {
				u1.recupImpot();
				System.out.println("La valeur du coffre est : "+u1.getCoffre());

				
			}
			else if(action ==3) {	
				if (u1.getCoffre() < 50) {
					System.out.println("Ce choix est impossible car vous n'avez pas au minimun 50 pi�ce");
				}
				
				else {
				Stuart b = new Stuart();
				u1.ajouterdansusineEmpire(b);
				System.out.println("La valeur du coffre est : "+u1.getCoffre());
				}

			}

		}

	}

}

