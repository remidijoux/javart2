package TP2_Java;

import java.util.Vector;

import TP1.Stuart;

public class Magasin {
	//Atributs

	//On cr�e un vecteur liste qui va contenir des objets Produits
	private Vector<Produit> liste;
	//On cr�e un attributs pour le capitale de type double
	protected double capitale = 10000;
	//On cr�e un attribut pour le nom de type String
	private String nom;

	//On cr�e un constructeur qui contiendar le nom du magasin
	public Magasin(String n) {
		nom = n;

		//On cr�e un vecteur qui contiendra la liste des produits
		liste = new Vector<Produit>();

	}

	//Accesseur

	//On cr�e un asseceur pour obtenir la valeur du capitale
	public double getCapitale() {
		return this.capitale;

	}

	//M�thode

	//On cr�e une m�thode pour ajouter un produit dans le magasin
	public void ajouterMagasin(Produit s) {
		liste.addElement(s);
		System.out.println(s.getNom()+ " est mis dans le magasin ");
	}

	//On cr�e une m�thode pour avoir la liste des produits disponible dans le magasin
	public void listerMagasin() {
		for (int i=0; i<liste.size(); i++) {
			System.out.println("\nVoici le produit qui est dans la liste du magasin : "+liste.elementAt(i).getNom());	

			System.out.println("\nVoici la quantit� qu'il reste pour ce produit : "+liste.elementAt(i).getQuantite());		
		}
	}




}
