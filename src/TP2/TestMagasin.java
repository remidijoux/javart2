package TP2_Java;

public class TestMagasin {

	public static void main(String[] args) {

		//Exercice 1.1
		System.out.println("Exercice 1.1");
		//On cr�e un objet de type t�l�vision

		AppareilElectro t1 = new AppareilElectro("Television", 100, 200, 1, 1123123);

		//On cr�e un objet de type Ananas
		FruitPiece a = new FruitPiece("Ananas", 70, 140, 3, "Maroc");
		//On cr�e un objet de type Banane
		FruitVrac b = new FruitVrac("Banane", 50, 100, 5000, "Portugal");

		//On pr�sente chaque objet 
		t1.presentation();
		a.presentation();
		b.presentation();

		//On ajoute au stock deux t�l�
		t1.ajouterStock(2);
		System.out.println("\nOn a une quantit� de "+t1.getNom()+ " de "+t1.getQuantite());

		//On supprime 1500kg de banane au stock
		b.supprimerQuantite(1500);
		System.out.println("\nOn a une quantit� de "+b.getNom()+ " de "+b.getQuantite());


		Magasin m = new Magasin("McDonald");

		m.ajouterMagasin(a);
		a.presentation();
		a.vendrePiece(1, m);
		a.presentation();

		m.ajouterMagasin(t1);
		m.ajouterMagasin(b);

		//Exercice 2.2
		System.out.println("\nExercice 2.2");

		//On enleve 800kg de banane 
		b.vendrePoids(800, m);
		b.presentation();
		//On enl�ve 3000kg de banane, mais �a ne marche pas
		b.vendrePoids(3000, m);


		//Exercice 3.2
		System.out.println("\nExercice 3.2");
		//On fait une solde de 20% sur un ananas
		a.vendreSolde(20);
		//On stop les soldes sur l'ananas
		a.stopSolde();

		//Exercice 4.2
		System.out.println("\nExercice 4");
		m.listerMagasin();


	}



}
