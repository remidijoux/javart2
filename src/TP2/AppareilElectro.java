package TP2_Java;

public class AppareilElectro extends Produit{

	//Attributs

	//On cr�e un attribut de type int pour le codebarre
	private int codebarre;
	//On cr�e un attribut de type int pour ajouter au stock un appareil
	private int ajouterStock=1;
	//On cr�e un attribut de type int pour faire sortie un appareil
	private int sortieStock=-1;	


	//On cr�e un constrcteur qui contiendra diff�rent attributs pour le nom, le prix d'achat, prix de vente et la quantit� et le code barre
	public AppareilElectro(String n, double pa, double pv, double q, int c) {
		super(n, pa, pv, q);
		this.codebarre = c;
	}

	//Accesseur

	//On cr�e un asseceur pour r�cup�rer la valeur du code barre
	public int getCodebar() {
		return this.codebarre;
	}

	//M�thode

	//On cr�e une m�thode pour pr�senter l'appareil
	public void presentation() {
		System.out.println("Ceci est un(e) "+getNom());
		System.out.println("Le prix d'achat est de "+getNom()+" est de "+getPrixVente());
		System.out.println("Le prix de vente est de "+getNom()+" est de "+getPrixAchat());
		System.out.println("La quantit� de "+getNom()+" est de "+getQuantite());
		System.out.println(getNom()+ " a comme code bar "+getCodebar());

	}

	//On cr�e une m�thode pour vendre un appareil en pi�ce
	public void vendrePiece(int q, Magasin m) {
		if(q>quantite) {
			System.out.println("Impossible d'enlever "+q+" "+getNom()+" car la quantit� que vous voulez enlever est sup�rieure � la quantit� iniatile de "+getNom());
		}
		else {
			this.quantite = quantite-q;
			System.out.println("On vend "+q+" "+getNom());	
			m.capitale = m.capitale + (this.prixVente*q);
			System.out.println("La valeur du capitale est de "+m.getCapitale());
		}


	}



}





