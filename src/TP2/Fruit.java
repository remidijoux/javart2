package TP2_Java;

public abstract class Fruit extends Produit{

	protected String pays;

	//On cr�e un constrcteur qui contiendra diff�rent attributs pour le nom, le prix d'achat, prix de vente et la quantit� et le pays d'origine
	public Fruit(String n, double pa, double pv, double q, String p) {
		super(n, pa, pv, q);

		this.pays=p;
	}

	//Accesseur

	//On cr�e un accesseur pour retourner la valeur du pays
	public String getPays() {
		return this.pays;
	}


	//M�thode

	//On cr�e une m�thode pour pr�senter un fruit
	public void presentation() {
		System.out.println("Ceci est un(e) "+getNom());
		System.out.println("Le prix d'achat est de "+getNom()+" est de "+getPrixVente());
		System.out.println("Le prix de vente est de "+getNom()+" est de "+getPrixAchat());
		System.out.println("La quantit� de "+getNom()+" est de "+getQuantite());
		System.out.println(getNom()+ " viens de "+getPays());

	}




}
