package TP3;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ConvertisseurEuroDollar extends JFrame implements ActionListener{
	//Attributs 

	//On crée un conteneur qui contiendra tout le programme
	private Container panneau;
	//On crée monBouton qui est de type JButton
	private JButton monBouton;
	//On crée un champTexte pour rentrer la valeur en dollard
	private JTextField monChampTexte;
	//On crée un nouveau label pour afficher la flèche qui donnera le type de conversion
	private JLabel monLabel;
	//On crée un champ texte pour écrire la valeur de l'euro
	private JTextField euroTexte;
	//On crée un label pour afficher un sigle "$"
	private JLabel tauxTexte;
	//On crée un label pour afficher un texte pour le taux
	private JLabel monLabeldollard;
	//On crée un JButton "monBoutoQuitter" pour ferme la fenetre
	private JButton monBoutonQuitter;
	//On crée un label pour écrire la valeur en dollard
	private JTextField dollardTexte;
	//On crée un label pour afficher un sigle "$"
	private JLabel monLabeldollard2;

	//On crée un attribut euro de type double
	private double euro;
	//On crée un attribut valeur de type double
	private double valeur;
	//On crée un attribut dollard de type double
	private double dollard;
	//On crée un attribut sensfleche iniatialise pour changer le type de conversion
	private double sensfleche=0;




	public ConvertisseurEuroDollar(){
		//On donne un nom à la fenetre
		super("Mon Application Graphique");
		//On définit les dimension de la fenetre
		setSize(300, 200);
		//On définit la location de la fenetre
		setLocation(20,20);
		//Cela va permettre de terminer l'application  la fermeture de la fentre
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//On va centre la fenetre au milieu de l'écran
		this.setLocationRelativeTo(null);


		//Récupération du container
		panneau = getContentPane();
		//On définit une taille pour le container
		panneau.setLayout(new GridLayout(3,6));

		//On crée trois nouveaux panel
		JPanel part1 = new JPanel();
		JPanel part2 = new JPanel();
		JPanel part3 = new JPanel();

		//On ajoute ces panels au conteneurs
		panneau.add(part1);
		panneau.add(part2);
		panneau.add(part3);

		//Création des objets à utiliser
		euroTexte = new JTextField("");
		monLabel = new JLabel(" € =>  ");
		monChampTexte = new JTextField(" ");
		monLabeldollard = new JLabel(" $ ");
		tauxTexte = new JLabel("Taux 1€ =");
		dollardTexte = new JTextField(" ");
		monLabeldollard2 = new JLabel(" $ ");
		monBouton = new JButton("Calculer");
		monBoutonQuitter = new JButton("Quitter");





		//On donne la dimension du premier panel part1
		part1.setLayout(new GridLayout(0,4));

		//On ajoute au pannel part1 les différents objets
		part1.add(euroTexte);
		part1.add(monLabel);
		part1.add(monChampTexte);
		part1.add(monLabeldollard);


		//On donne la dimension du deuxième panel
		//On ajoute au pannel part2 les différents objets
		part2.setLayout(new GridLayout(0,5));
		part2.add(tauxTexte);
		part2.add(dollardTexte);
		part2.add(monLabeldollard2);

		//On donne les dimension du pannel part3
		//On ajoute au panel part3 les différents objets
		part3.setLayout(new GridLayout(0,2));
		part3.add(monBouton);
		part3.add(monBoutonQuitter);

		//On ajoute la possibilité de detecter l'action des deux bouton lorsque l'on clique dessus
		monBoutonQuitter.addActionListener(this);
		monBouton.addActionListener(this);

		//On ajoute la possibilité de faire une action lorsque l'on clique sur un label avec MouseListerner
		monLabel.addMouseListener(new MouseListener() {


			//Lorsque que l'on clique sur un label la valeur changera
			public void mouseClicked(MouseEvent e) {

				monLabel.setText("€ <=");
				sensfleche = 1;

			}

			public void mouseEntered(MouseEvent arg0) {

			}

			public void mouseExited(MouseEvent arg0) {

			}

			public void mousePressed(MouseEvent arg0) {

			}
			
			//Lorsque l'on clique sur un label la valeur changera donc on pourra changer le type de conversion
			public void mouseReleased(MouseEvent arg0) {
				monLabel.setText("€ =>");
				sensfleche=0;
			}
		});

		//On va pouvoir voir la fenetre
		setVisible(true);


	}

	//On ici la partie pour executer le programme
	public static void main(String[] args) {
		ConvertisseurEuroDollar n = new ConvertisseurEuroDollar();
	}


	//On utilise une methode pour utilser les bouton en listenner
	public void actionPerformed(ActionEvent e) {
		
		//On Ajoute une méthode pour définir une action sur "monBouton" autrement dit ce bouton va faire le calcul
		if (e.getSource() == monBouton) {
			//Si valeur de la fleche =0  alors on change le sens de la conversion (soit euro vers dollard)
			if(sensfleche == 0) {
				calculerConversion(Double.parseDouble(euroTexte.getText()), Double.parseDouble(dollardTexte.getText())) ;
			}
            //Si valeur de la fleche =1  alors on change le sens de la conversion (soit dollard vers euro)
			if(sensfleche == 1) {
				calculerConversionInverse(Double.parseDouble(monChampTexte.getText()), Double.parseDouble(dollardTexte.getText())) ;
			}
		}
		
		//Lorsque l'on clique sur le bouton "monbBoutonQuitter" alors cela fermera la fenetre
		if (e.getSource() == monBoutonQuitter) {
			System.exit(0);
		}
	}

	//On crée une méthode pour convertir les euro en dollard
	public void calculerConversion(double eu, double val) {
		this.euro = eu;
		this.valeur = val;
		dollard = euro*valeur;
		monChampTexte.setText(""+dollard);


	}

	//On crée une méthode pour convertir les dollard en euro
	public void calculerConversionInverse(double val, double doll) {
		this.dollard = val;
		this.valeur = doll;
		euro =dollard/valeur;
		euroTexte.setText(""+euro);
	}




}
